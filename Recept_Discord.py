# bot.py
import os
import discord
import time
from Send_Discord import SendMsgDiscord
from dotenv import load_dotenv

#https://discordpy.readthedocs.io/en/latest/quickstart.html
load_dotenv()
token = os.getenv('DISCORD_TOKEN')

client = discord.Client()

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content == '#start':
        #lance un .bat qui éteint le serveur
        os.startfile("C:\GTAserver\FXServer\closeServeur.bat")
        time.sleep (5)
        #lance un .bat qui execute le serveur
        os.startfile("C:\GTAserver\FXServer\GTAServeur.bat")
        #affiche le lancement du serveur
        SendMsgDiscord("serveur GTA en cours de chargement")
        return

    if message.content == '#stop':
        #lance un .bat qui éteint le serveur
        os.startfile("C:\GTAserver\FXServer\closeServeur.bat")
        #affiche l'extinction du serveur
        SendMsgDiscord("serveur éteint")
        return

    if message.content == '#maj':
        #lance un .bat qui éteint le serveur
        os.startfile("C:\GTAserver\FXServer\closeServeur.bat")
        time.sleep (5)
        #lance un .bat qui execute le serveur
        os.startfile("C:\GTAserver\FXServer\PullAll.bat")
        time.sleep (5)
        #lance un .bat qui execute le serveur
        os.startfile("C:\GTAserver\FXServer\GTAServeur.bat")
        #affiche le lancement du serveur
        SendMsgDiscord("serveur mis à jour et en cours de chargement")
        return
    if message.content in ['#help', 'help', 'aide']:
        SendMsgDiscord("**veuillez entrer une des commandes suivantes :** /n - **#start** -> pour démarrer le serveur /n - **#stop** -> pour éteindre le serveur /n - **#maj** -> pour mettre à jour le serveur (Cela fait aussi redémarrer le serveur)")
client.run(token)
