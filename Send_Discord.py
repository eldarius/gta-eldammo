#!/usr/bin/env python3

#Import
import json
import time

from urllib import request
from urllib.error import HTTPError
#Fonction
def SendMsgDiscord(Msg):
    time.sleep(10)
    #WEBHOOK : bot https://discordapp.com/api/webhooks/701033471128371241/6QKEK5ihjW4__TI5jnFW_d5qnvfMTpjIIczy_pByGk7UtiVB8LCMnqZeN7rzTN2QHL4m
    WEBHOOK_URL = 'https://discordapp.com/api/webhooks/725777554157338635/b29GIrjhjOzrejwKqvAM_8UUPWsJ-hxgDYhIQ41gIaINwvPkz7C7DfLa8dwzrUGcXoI6'

    # La payload

    payload = {
        'content': Msg
    }

    # Les paramètres d'en-tête de la requête
    headers = {
        'Content-Type': 'application/json',
        'user-agent': 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'
    }

    # Enfin on construit notre requête
    req = request.Request(url=WEBHOOK_URL,
                        data=json.dumps(payload).encode('utf-8'),
                        headers=headers,
                        method='POST')

    # Puis on l'émet !
    try:
        response = request.urlopen(req)
        print(response.status)
        print(response.reason)
        print(response.headers)
        time.sleep(10)
    except HTTPError as e:
        print('ERROR')
        print(e.reason)
        print(e.hdrs)
        print(e.file.read())
    
