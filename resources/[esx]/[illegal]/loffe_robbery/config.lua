Config = {}
Translation = {}

Config.Shopkeeper = 416176080 -- hash of the shopkeeper ped
Config.Locale = 'custom' -- 'en', 'sv' or 'custom'

Config.Shops = {
    -- {coords = vector3(x, y, z), heading = peds heading, money = {min, max}, cops = amount of cops required to rob, blip = true: add blip on map false: don't add blip, name = name of the store (when cops get alarm, blip name etc)}
    {coords = vector3(24.03, -1345.63, 29.5-0.98), heading = 266.0, money = {2000, 3200}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(-705.73, -914.91, 19.22-0.98), heading = 91.0, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(1391.44, 3606.26, 34.9-0.98), heading = 210.3, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(1697.34, 4923.12, 42.06-0.98), heading = 325.39, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(1727.64, 6415.6, 35.03-0.98), heading = 236.01, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(-2965.82, 391.27, 15.04-0.98), heading = 85.35, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(2677.84, 3279.28, 55.24-0.98), heading = 326.74, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(549.41, 2671.29, 42.15-0.98), heading = 96.01, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(-1819.07, 793.86, 138.07-0.98), heading = 133.04, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(372.31, 326.44, 103.56-0.98), heading = 260.32, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(1165.25, -323.46, 69.2-0.98), heading = 110.18, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(2557.12, 380.45, 108.62-0.98), heading = 358.23, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(1133.83, -983.17, 46.41-0.98), heading = 272.14, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(-47, -1758.95, 29.42-0.98), heading = 48.23, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(-1221.04, -908.26, 12.32-0.98), heading = 39.11, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false},
    {coords = vector3(-1486.44, -377.22, 40.16-0.98), heading = 127.77, money = {2500, 3800}, cops = 0, blip = false, name = 'Braquage Superette', cooldown = {hour = 0, minute = 30, second = 0}, robbed = false}

}

Translation = {
    ['en'] = {
        ['shopkeeper'] = 'shopkeeper',
        ['robbed'] = "I was just robbed and ~r~don't ~w~have any money left!",
        ['cashrecieved'] = 'You got:',
        ['currency'] = '$',
        ['scared'] = 'Scared:',
        ['no_cops'] = 'There are ~r~not~w~ enough cops online!',
        ['cop_msg'] = 'We have sent a photo of the robber taken by the CCTV camera!',
        ['set_waypoint'] = 'Set waypoint to the store',
        ['hide_box'] = 'Close this box',
        ['robbery'] = 'Robbery in progress',
        ['walked_too_far'] = 'You walked too far away!'
    },
    ['sv'] = {
        ['shopkeeper'] = 'butiksbiträde',
        ['robbed'] = 'Jag blev precis rånad och har inga pengar kvar!',
        ['cashrecieved'] = 'Du fick:',
        ['currency'] = 'SEK',
        ['scared'] = 'Rädd:',
        ['no_cops'] = 'Det är inte tillräckligt med poliser online!',
        ['cop_msg'] = 'Vi har skickat en bild på rånaren från övervakningskamerorna!',
        ['set_waypoint'] = 'Sätt GPS punkt på butiken',
        ['hide_box'] = 'Stäng denna rutan',
        ['robbery'] = 'Pågående butiksrån',
        ['walked_too_far'] = 'Du gick för långt bort!'
    },
    ['custom'] = { -- edit this to your language
        ['shopkeeper'] = 'Commerçant',
        ['robbed'] = 'On vient de me voler et je ~r~n\'ai plus ~w~d\'argent !',
        ['cashrecieved'] = 'Vous avez :',
        ['currency'] = '$',
        ['scared'] = 'Effrayé :',
        ['no_cops'] = 'Il n\'y a ~r~pas~w~ assez de flics en ligne !',
        ['cop_msg'] = 'Nous avons envoyé une photo du voleur prise par la caméra de vidéosurveillance !',
        ['set_waypoint'] = 'Définir le waypoint jusqu\'au magasin',
        ['hide_box'] = 'Fermer cette boîte',
        ['robbery'] = 'Braquage en cours',
        ['walked_too_far'] = 'Tu es parti trop loin !'
    }
}