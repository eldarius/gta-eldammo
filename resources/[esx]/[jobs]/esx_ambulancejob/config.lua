Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerColor                = { r = 102, g = 0, b = 102 }
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.ReviveReward               = 0  -- revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog              = true -- enable anti-combat logging?
Config.LoadIpl                    = true -- disable if you're using fivem-ipl or other IPL loaders
Config.Locale = 'fr'

local second = 1000
local minute = 60 * second

-- How much time before auto respawn at hospital
Config.RespawnDelayAfterRPDeath   = 10 * minute

-- How much time before a menu opens to ask the player if he wants to respawn at hospital now
-- The player is not obliged to select YES, but he will be auto respawn
-- at the end of RespawnDelayAfterRPDeath just above.
Config.RespawnToHospitalMenuTimer   = true
Config.MenuRespawnToHospitalDelay   = 5 * minute

Config.EnablePlayerManagement       = true
Config.EnableSocietyOwnedVehicles   = false

Config.RemoveWeaponsAfterRPDeath    = true
Config.RemoveCashAfterRPDeath       = true
Config.RemoveItemsAfterRPDeath      = false

-- Will display a timer that shows RespawnDelayAfterRPDeath time remaining
Config.ShowDeathTimer               = true

-- Will allow to respawn at any time, don't use with RespawnToHospitalMenuTimer enabled!
Config.EarlyRespawn                 = false
-- The player can have a fine (on bank account)
Config.RespawnFine                  = false
Config.RespawnFineAmount            = 500

Config.Blip = {
	Pos     = { x = 290.26, y = -588.49, z = 43.18 },
	Sprite  = 61,
	Display = 4,
	Scale   = 1.2,
	Colour  = 2,
}

Config.HelicopterSpawner = {
	SpawnPoint = { x = 313.33, y = -1465.2, z = 45.5 },
	Heading    = 0.0
}

-- https://wiki.fivem.net/wiki/Vehicles
Config.AuthorizedVehicles = {

	{
		model = 'ambulance',
		label = 'Ambulance'
	},
	{
		model = 'dodgeEMS',
		label = 'Dodge'
	}

}

Config.Zones = {

	HospitalInteriorEntering1 = { -- Main entrance
		Pos	= { x = 319.96, y = -559.44, z = 28.0 },
		Type = 1
	},

	HospitalInteriorInside1 = {  -- point d'arriver apares tp entrée
		Pos	= { x = 337.42, y = -592.63, z = 42.6 },
		Type = -1
	},

	HospitalInteriorOutside1 = {  -- point d'arriver apares tp sortie
		Pos	= { x = 319.15, y = -556.84, z = 28.0 },
		Type = -1
	},

	HospitalInteriorExit1 = {   
		Pos	= { x = 338.97, y = -592.36, z = 42.6 },
		Type = 1
	},

	--HospitalInteriorEntering2 = { -- Lift go to the roof
	--	Pos	= { x = 247.1, y = -1371.4, z = 23.5 },
	--	Type = 1
	--},

	--HospitalInteriorInside2 = { -- Roof outlet
	--	Pos	= { x = 333.1,	y = -1434.9, z = 45.5 },
	--	Type = -1
	--},

	--HospitalInteriorOutside2 = { -- Lift back from roof
	--	Pos	= { x = 249.1,	y = -1369.6, z = 23.5 },
	--	Type = -1
	--},

	--HospitalInteriorExit2 = { -- Roof entrance
	--	Pos	= { x = 335.5, y = -1432.0, z = 45.5 },
	--	Type = 1
	--},

	AmbulanceActions = { -- Cloakroom
		Pos	= { x = 338.6, y = -575.5, z = 42.6 },
		Type = 1
	},

	VehicleSpawner = {
		Pos	= { x = 323.40, y = -556.72, z = 28.0 },
		Type = 36
	},

	VehicleSpawnPoint = {
		Pos	= { x = 325.63, y = -548.92, z = 22.6 },
		Type = -1
	},

	VehicleDeleter = {
		Pos	= { x = 339.50, y = -560.97, z = 28.0 },
		Type = 1
	},

	Pharmacy = {
		Pos	= { x = 323.08, y = -570.37, z = 42.6 },
		Type = 1
	}

	--ParkingDoorGoOutInside = {
	--	Pos	= { x = 234.56, y = -1373.77, z = 20.97 },
	--	Type = 1
	--},

	--ParkingDoorGoOutOutside = {
	--	Pos	= { x = 320.98, y = -1478.62, z = 28.81 },
	--	Type = -1
	--},

	--ParkingDoorGoInInside = {
	--	Pos	= { x = 238.64, y = -1368.48, z = 23.53 },
	--	Type = -1
	--},

	--ParkingDoorGoInOutside = {
	--	Pos	= { x = 317.97, y = -1476.13, z = 28.97 },
	--	Type = 1
	--},

	--StairsGoTopTop = {
	--	Pos	= { x = 251.91, y = -1363.3, z = 38.53 },
	--	Type = -1
	--},

	--StairsGoTopBottom = {
	--	Pos	= { x = 237.45, y = -1373.89, z = 26.30 },
	--	Type = -1
	--},

	--StairsGoBottomTop = {
	--	Pos	= { x = 256.58, y = -1357.7, z = 37.30 },
	--	Type = -1
	--},

	--StairsGoBottomBottom = {
	--	Pos	= { x = 235.45, y = -1372.89, z = 26.30 },
	--	Type = -1
	--}

}